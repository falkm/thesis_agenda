                   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                    PROSPECT: PHD PIGLET LOCOMOTION

                              Falk Mielke
                   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


                               2022-06-13


Table of Contents
─────────────────

1. General Introduction
2. Digitization Methodology
3. Fourier Analysis in Kinematics
.. 1. Introduction of FCAS
.. 2. Review: Fourier Series in Kinematics
4. Developmental Kinematics
5. Inverse Modeling
6. CT Inertials
7. General Discussion
8. Appendix
.. 1. Inverse Dynamics
.. 2. Open Source XROMM
.. 3. Piglet Head Movement (JG collaboration)
.. 4. Link to Energetics
.. 5. Technical Notes


To give an outlook what is to be expected, I here outline the chapters
we plan for the thesis to come and report on their status.


1 General Introduction
══════════════════════

  ⁃ Status: in preparation
  ⁃ <https://git.sr.ht/~falk/thesis>

  The thesis will focus on methodological advances in measuring piglet
  locomotion, focusing on the LBW/NBW comparison. The general intro will
  briefly summarize relevant prior work, and anticipate trajectories for
  improvement.


2 Digitization Methodology
══════════════════════════

  ⁃ Status: done (minor refinements)
  ⁃ <https://doi.org/10.1242/bio.055962>
  ⁃ “tracking” data set

  Though this manuscript was written by my wife, we agreed that it is of
  major relevance for my thesis. I coordinated and contributed to the MS
  and did most of the programming work. Maja has confirmed that this is
  used for my thesis.

  I might add a brief report on the actual experience with DeepLabCut.


3 Fourier Analysis in Kinematics
════════════════════════════════

3.1 Introduction of FCAS
────────────────────────

  ⁃ Status: done
  ⁃ <https://doi.org/10.1093/zoolinnean/zlz135>
  ⁃ “youtube” data set

  This manuscript was the initiation of the Fourier Series analysis I
  suggest for kinematics of cyclic locomotor phenomena. The appendix
  holds some relevant formulas; the ms itself is a basic demonstration
  of how to enable very simple multivariate analyses in the frequency
  domain.


3.2 Review: Fourier Series in Kinematics
────────────────────────────────────────

  ⁃ Status: in progress
  ⁃ <https://git.sr.ht/~falk/papio_fcas/tree/master/item/ms3_fourier>
  ⁃ literature review

  This will provide an overview on the “history of Fourier methods in
  kinematics”. Sounds more boring than it is, because luckily people
  have not understood the concept and done a lot wrong (regression
  instead of transformation, FFT instead of FSD, using only “sin” and no
  “cos” in the trigonometric formulation, coefficient count). I think
  this will be a valuable summary for future researchers, and it is
  “cheap” to get.

  This chapter might either be standalone or combined with the
  “Developmental Kinematics”, below.


4 Developmental Kinematics
══════════════════════════

  ⁃ Status: Analysis done; writing required
  ⁃ <https://git.sr.ht/~falk/papio_fcas/tree/master/item/ms2_figures>
  ⁃ “baboon” data / <https://doi.org/10.1002/ajpa.24454>

  Work on this data set was initiated because it is a simple,
  well-understood reference case for studying development in locomotor
  kinematics. This will re-iterate the Fourier Analysis and extend it by
  applying a first probabilistic model (conventional model structure).

  The focus of this chapter is to convince the reader that variability
  is a crucial feature of the phenomenon we are studying.


5 Inverse Modeling
══════════════════

  ⁃ Status: done
  ⁃ <https://doi.org/10.1101/2022.02.04.479126>
  ⁃ piglet 2D kinematics data

  This manuscript builds upon the baboon study above: sample size is
  much higher, and the model is a step up in complexity.


6 CT Inertials
══════════════

  ⁃ Status: analysis done, writing required
  ⁃ <http://mielke-bio.info/falk/posts/26.seb2021>
  ⁃ “femur+plastic” microCT data

  This part will be a written report of what I discussed in a 2021
  conference talk. Inertial properties are causally related to the
  movements we observe, hence this chapter is the link to the (appended)
  studies on inverse dynamics.


7 General Discussion
════════════════════

  ⁃ Status: not started

  This will summarize the achievements of the thesis, link to other
  research (e.g. the XROMM, see appendix), highlight future challenges.


8 Appendix
══════════

  (some ideas and available materials; more might come)


8.1 Inverse Dynamics
────────────────────

  The logical next step.


8.2 Open Source XROMM
─────────────────────

  … as I achieved it with `python` and `blender`.


8.3 Piglet Head Movement (JG collaboration)
───────────────────────────────────────────

  … as another indication that we are all just oscillating rigid bodies.


8.4 Link to Energetics
──────────────────────

  “Swing it like a piglet” talk, SICB2021
  (<http://mielke-bio.info/falk/posts/29.sicb2021>)


8.5 Technical Notes
───────────────────

  All the mathematical details on Fourier Series, Quaternions, Wrenches,
  Epipolar Geometry, Force Plate Electronics, etc. which I prefer to
  spare the readers of the main text. Most of these are already present
  in rough form on my blog: <http://mielke-bio.info/falk>
